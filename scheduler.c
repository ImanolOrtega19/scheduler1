#include "scheduler.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define init_time_task  0

int main(void)
{
	
	int option, num_tasks=0;
	
	do{
		printf("SCHEDULER\n");
		printf("1.- NUEVA TAREA\n");
		printf("2.- ENLISTAR TAREAS\n");
		printf("3.- INICIAR TAREAS\n");
		printf("4.- VACIAR LISTA DE TAREAS\n");
		printf("5.- SALIR\n");
		printf("|=|\n");	
		printf("Ingresa el numero que quiere realizar:");
		scanf("%d", &option);
		switch (option)
		{
			case 1:
			{
				printf("¿Cuantas tareas quiere enlistar?:");
				scanf("%d", &num_tasks);				
				UploadDataTotask(num_tasks);					
				break;
			}
			case 2:
			{
				printf("LISTA DE PROCESOS\n");
				for (int k = 0; k < num_tasks; k++)
				{
					if (task[k].time_execute == 0)
					{
						
						printf("|+++++++++++++++++++++++++++++++++++++++++++++++++++++++|\n");
						printf("|=TAREA[%i]\n", k);
						printf("|=TAREA ID: %d\n",task[k].id);
						printf("|=NOMBRE DE LA TAREA: %s\n",task[k].name_task);
						printf("|=PRIORIDAD DE LA TAREA: %d\n",task[k].priority);
						printf("|=TIEMPO DE EJECUCION DE LA TAREA: %d\n",task[k].time_execute);
						printf("|+++++++++++++++++++++++++++++++++++++++++++++++++++++++|\n\n");
					}
				}
				printf("PROCESOS EN ESPERA DE EJECUCION\n");
				for (int k = 0; k < num_tasks; k++)
				{
					if (task[k].time_execute > 0)
					{
						
						printf("|++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++|\n");
						printf("|=TAREA[%i]\n", k);
						printf("|=TAREA ID: %d\n",task[k].id);
						printf("|=NOMBRE DE LA TAREA: %s\n",task[k].name_task);
						printf("|=PRIORIDAD DEL TAREA: %d\n",task[k].priority);
						printf("|=TIEMPO DE EJECUCION: %d\n",task[k].time_execute);
						printf("|+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++|\n\n");
					}
				}

				break;
			}
			case 3:
			{
				char flag[20] = "FINALIZADO";
				for (int i = 0; i < num_tasks; i++)
				{
					if (task[i].time_execute==0)
					{
						printf("NO HAY PROCESOS EN COLA DE ESPERA\n");
						break;
					}else
					{


						printf("ORDENANDO DE TAREAS POR PRIORIDAD DE MAYOR A MENOR\n");
						SortArray(num_tasks);
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("OK\n");
						for (int i = 0; i < 50; i++)
						{
							delay(2);
						}
						system("clear");
						printf("IMPRIMIENDO PRIORIDAD ORDENADA\n");
						//IMPRIMIR PRIORIDAD ORDENADA MAYOR A MENOR

						for (int i = 0; i < num_tasks; i++)
						{
							printf("|**********************************************************|\n");
							printf("|ID TAREA: [%d]\n", task[i].id);
							printf("|NOMBRE DE LA TAREA: [%s]\n",task[i].name_task);
							printf("|PRIORIDAD DEL TAREA: [%d]\n", task[i].priority);
							printf("|TIEMPO DE EJECUCION DE LA TAREA: [%d]\n",task[i].time_execute);
							printf("|************************************************************\n");
						}
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("INICIANDO EJECUCION DE TAREAS\n");
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("POR FAVOR ESPERE\n");
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("YA ESTAAAA\n");
						for (int i = 0; i < num_tasks; i++)
						{
							do{
								printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
								printf("|ID DE LA TAREA : [%d]\n", task[i].id);
								printf("|NOMBRE DE LA TAREA: [%s]\n",task[i].name_task);
								printf("|PRIORIDAD DE LA TAREA: [%d]\n", task[i].priority);
								printf("|TIEMPO DE EJECUCION DE LA TAREA: [%d]\n",task[i].time_execute);
								task[i].time_execute = task[i].time_execute - 1;
								printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
								for (int i = 0; i < 50; i++)
								{
									delay(3);
								}
								system("clear");
							}while(task[i].time_execute != 0);
						}
						printf("TAREAS FINALIZADAS: \n");
							for (int i = 0; i < 50; i++)
								{
									delay(3);
								}
							for (int i = 0; i < num_tasks; i++)
							{
								printf("|++++++++++++++++++++++++++++++++++++++++++++++++++++++++|\n");
								printf("|ID TAREA: [%d]\n", task[i].id);
								printf("|NOMBRE DEL TAREA: [%s]\n",task[i].name_task);
								printf("|PRIORIDAD DE LA TAREA: [%d]\n", task[i].priority);
								printf("|TIEMPO DE EJECUCION DE LA TAREA: [%s]\n",flag);
								printf("|++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
							}


					}
				}
				
				
				

				break;
			}
			case 4:
			{
				EmptyListTask(num_tasks);
				printf("YA ESTAAA\n");
				for (int i = 0; i < 50; i++)
				{
					delay(3);
				}
				num_tasks = 0;
				system("clear");	
				break;
			}
			case 5:
			{
				printf("TERMINANDO TAREAS...");
				for (int i = 0; i < 50; i++)
				{
					delay(5);
					printf(".\n");	
				}
				system("clear");
				break;
			}
			default :
			{
				printf("LO SENTIMOS PERO LA OPCION INGRESADA [%d] ES INVALIDA\n", option);
				for (int i = 0; i < 50; i++)
				{
					delay(5);
				}
				system("clear");
				break;
			}
		}
	}while(option != 5);	
	return 0;
}


void delay(int num_of_seconds)
{
	int miliseconds = 10000 * num_of_seconds;
	clock_t start_time = clock();
	while(clock() < start_time + miliseconds);
}

void UploadDataTotask(int num_tasks)
{
	for (int i = 0; i < num_tasks; i++)
	{
		printf("T A S K [%d]\n", i);
		printf("Ingresa el id de la tarea: ");
		scanf("%i", &task[i].id);
		printf("Ingresa el nombre de la tarea: ");
		scanf("%s", &task[i].name_task);
		printf("Ingresa la prioridad de la tarea: ");
		scanf("%d", &task[i].priority);
		printf("Ingresa el tiempo que deseas ejecutar la tarea: (seg)");
		scanf("%d", &task[i].time_execute);
	}
}

void EmptyListTask(int num_tasks){
	
	for (int i = 0; i < 100; i++)
	{
		task[i].id = 0;
		strcpy(task[i].name_task,"");
		task[i].priority = 0;
		task[i].time_execute = 0;
	}
}

void SortArray(int num_tasks){
	int temporary, temprary2, temporary3;
	char temporal4[50];

	for (int i = 0;i < num_tasks; i++){
		for (int j = 0; j< num_tasks-1; j++){
			if (task[j].priority < task[j+1].priority){
			temporary = task[j].priority; 
			temprary2= task[j].id;
			temporary3=task[j].time_execute;
			strcpy(temporal4,task[j].name_task);
			task[j].priority = task[j+1].priority; 
			task[j].id = task[j+1].id;
			task[j].time_execute=task[j+1].time_execute;
			strcpy(task[j].name_task,task[j+1].name_task);
			task[j+1].priority = temporary;
			task[j+1].id=temprary2;
			task[j+1].time_execute=temporary3;
			strcpy(task[j+1].name_task,temporal4);
			}
		}
	}
}